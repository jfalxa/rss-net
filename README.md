# RSS-NET

- Make as task manager
- Docker env for dev/prod
- gitlab-ci as CI
- go modules for deps management
- chi as http router
- sqlite with sqlx as database
- esbuild as typescript bundler
- preact as frontend framework

## Configuration

### Setup the host

Setup the HOST variable in an `.env` file at the root of the repo (see `.env.template`).
In your dev environment, you should add a corresponding line in your `/etc/hosts` to manage the main domain and the `www.`, `api.` and `auth.` subdomains:

```
# .env file
HOST=my_host.local
```

```
# /etc/hosts
# Static table lookup for hostnames.
# See hosts(5) for details.
127.0.0.1   localhost
::1         localhost
0.0.0.0     my_host.local www.my_host.local api.my_host.local auth.my_host.local
```

Now you'll be able to access the app at `http://my_host.local`, the api will be available under `http://api.my_host.local`.


### Setup SMTP provider

Grab the following information from your email service provider and add it to the `.env` file:

```
SMTP_HOST=smtp-provider.com
SMTP_PORT=587
SMTP_FROM=sender@email.com
SMTP_LOGIN=login@email.com
SMTP_PASSWORD=secret_password
```

### Setup JWT
 
You need to specify a JWT secret so the services can share JWT tokens and validate their signature.
To do so, create a random key and add it to your `.env` file:

```
JWT_SECRET=my_jwt_secret
```


## Commands

- `make` or `make run` to compile the binary and run it with Docker in background.
- `make down` to unmount the containers

## Architecture

- `rss-auth`: authentication and authorization api server
- `rss-resource`: resource api server
- `rss-web`: frontend app to display data from the api
- `rss-proxy`: nginx reverse proxy to control access to the other containers
