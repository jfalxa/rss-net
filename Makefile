run: down
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up --build -d

build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build

up: build
	docker-compose up -d

down:
	docker-compose down

registry:
	docker tag rss-auth:latest $(REGISTRY)/rss-auth:latest
	docker tag rss-resource:latest $(REGISTRY)/rss-resource:latest
	docker tag rss-proxy:latest $(REGISTRY)/rss-proxy:latest
	docker push $(REGISTRY)/rss-auth:latest
	docker push $(REGISTRY)/rss-resource:latest
	docker push $(REGISTRY)/rss-proxy:latest

production:
	docker-compose -f docker-compose.prod.yml pull
	docker-compose -f docker-compose.prod.yml down
	docker-compose -f docker-compose.prod.yml up -d
