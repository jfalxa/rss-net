import { useCallback, useEffect, useRef, useState } from "preact/hooks";

export interface Snapshot<T = any> {
	loading: boolean;
	data: T | null;
	error: Error | null;
}

export type PromiseHook<T extends any[], U> = [
	Snapshot<U>,
	(...args: T) => Promise<U | void>
];

function usePromise<T extends any[], U>(
	request: (...a: T) => Promise<U>,
	deps: any[] = []
): PromiseHook<T, U> {
	const [snapshot, setSnapshot] = useState<Snapshot<U>>({
		loading: false,
		data: null,
		error: null,
	});

	const cancel = useRef<() => boolean>();

	const resolve = useCallback(
		(...args: T) => {
			let active = true;

			// cancel the previous request if there was one
			cancel.current && cancel.current();
			// create the cancel function for this request
			cancel.current = () => (active = false);

			async function resolver() {
				if (active) {
					setSnapshot((s) => ({
						data: s.data,
						error: s.error,
						loading: true,
					}));
				}

				try {
					const res = await request(...args);

					if (active) {
						setSnapshot({
							loading: false,
							data: res ?? null,
							error: null,
						});
					}

					return res;
				} catch (error) {
					if (active) {
						setSnapshot({
							loading: false,
							data: null,
							error,
						});
					}
				}
			}

			return resolver();
		},
		[...deps]
	);

	useEffect(() => {
		return () => cancel.current && cancel.current();
	}, []);

	return [snapshot, resolve];
}

export default usePromise;
