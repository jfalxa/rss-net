export interface User {
	user_id: number;
	email: string;
}

export interface Feed {
	title: string;
	description: string;
	feed_url: string;
	icon: string;
	is_synced: boolean;
	is_deleted: boolean;
}

export interface Article {
	title: string;
	url: string;
	summary: string;
	image: string;
	date_published: string;
}

export interface FeedArticle {
	id?: number;
	feed: Feed["feed_url"];
	article: Article["url"];
}

export interface JSONFeed extends Feed {
	items: Article[];
}

export interface SyncOp {
	type: "add" | "remove";
	data: string;
}
