import { h, render } from "preact";
import { Redirect, Route } from "wouter-preact";

import "./index.css";

import Auth, { useAuth } from "./components/auth";
import { SearchForm, SearchResults, useSearch } from "./components/search";
import Feeds, { useFeeds } from "./components/feeds";
import Articles, { useArticles } from "./components/articles";
import Nav from "./components/nav";
import { Header, Top } from "./components/system";

const App = () => {
	const auth = useAuth();
	const search = useSearch();
	const feeds = useFeeds(auth);
	const articles = useArticles(feeds);

	return (
		<div class="app">
			<Nav user={auth.user} />

			<Route path="/">
				<Redirect to="/articles" />
			</Route>

			<Route path="/logout">
				<Auth.Logout
					canceling={auth.canceling}
					onCancelToken={auth.cancelToken}
				/>
			</Route>

			<Route path="/login">
				<main>
					<Header title="Login" />

					<Auth.Login
						step={auth.step}
						otping={auth.otping}
						tokening={auth.tokening}
						onRequestOTP={auth.requestOTP}
						onRequestToken={auth.requestToken}
					/>
				</main>
			</Route>

			<Route path="/feeds">
				<main>
					<Header title="Feeds">
						<SearchForm
							query={search.query}
							onChange={search.onChange}
							onSearch={search.onSearch}
						/>
					</Header>

					<Feeds list={feeds.list} onForget={feeds.forgetFeed} />
				</main>
			</Route>

			<Route path="/feeds/search">
				<main>
					<Header title="Search">
						<SearchForm
							query={search.query}
							onChange={search.onChange}
							onSearch={search.onSearch}
						/>
					</Header>

					<SearchResults
						query={search.query}
						results={search.results}
						saving={feeds.saving}
						onSave={feeds.saveFeed}
					/>
				</main>
			</Route>

			<Route path="/articles">
				<main>
					<Header title="Articles" />
					<Articles list={articles.list} />
					<Top />
				</main>
			</Route>
		</div>
	);
};

render(<App />, document.getElementById("root")!);
