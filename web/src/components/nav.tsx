import { h } from "preact";
import { Link } from "wouter-preact";
import cl from "clsx";

import "./nav.css";

import { User } from "../models";
import { Snapshot } from "../hooks/promise";
import { useState } from "preact/hooks";

const MenuIcon = (props: h.JSX.SVGAttributes<SVGSVGElement>) => (
	<svg {...props} width="32" height="32" viewBox="0 0 32 32">
		<g stroke="black" stroke-width={4}>
			<line x1="4" y1="7" x2="28" y2="7" />
			<line x1="4" y1="16" x2="28" y2="16" />
			<line x1="4" y1="25" x2="28" y2="25" />
		</g>
	</svg>
);

type NavProps = {
	user: Snapshot<User>;
};

const Nav = ({ user }: NavProps) => {
	const [open, setOpen] = useState(false);

	return (
		<nav class={cl("nav", open && "nav--open")}>
			<a class="nav__menu" title="Menu" onClick={() => setOpen(!open)}>
				<MenuIcon />
				{open && <h1 class="nav__title">[feed]</h1>}
			</a>

			<Link class="nav__link" to="/feeds" title="Feeds">
				[F{open && `eeds`}]
			</Link>

			<Link class="nav__link" to="/articles" title="Articles">
				[A{open && `rticles`}]
			</Link>

			{user.data ? (
				<Link class="nav__link" to="/logout" title={user.data.email}>
					[L{open && `ogout`}]
				</Link>
			) : (
				<Link class="nav__link" to="/login" title="Login">
					[L{open && `ogin`}]
				</Link>
			)}
		</nav>
	);
};

export default Nav;
