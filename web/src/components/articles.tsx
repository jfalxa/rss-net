import { h } from "preact";
import { useEffect } from "preact/hooks";

import "./articles.css";

import { Article } from "../models";
import { FeedsHook } from "./feeds";
import * as db from "../services/db";
import * as api from "../services/api";
import usePromise, { Snapshot } from "../hooks/promise";
import { List, Log } from "./system";

export interface ArticlesHook {
	list: Snapshot<Article[]>;
	loadArticles: () => Promise<Article[] | void>;
}

export function useArticles(feeds: FeedsHook): ArticlesHook {
	const feedList = feeds.list.data;

	const [list, loadArticles] = usePromise(async () => {
		for (const feed of feedList ?? []) {
			const res = await api.readFeed(feed.feed_url);
			await db.addArticles(res, res.items);
		}

		return db.getAllArticles();
	}, [feedList]);

	useEffect(() => {
		loadArticles();
	}, [loadArticles]);

	return {
		list,
		loadArticles,
	};
}

const dateDesc = (a: Article, b: Article) => {
	const dateA = new Date(a.date_published);
	const dateB = new Date(b.date_published);
	return dateB.getTime() - dateA.getTime();
};

type ArticleInfoProps = {
	article: Article;
};

const ArticleInfo = ({ article }: ArticleInfoProps) => {
	const source = new URL(article.url);
	const date = new Date(article.date_published).toUTCString();

	function openOrigin(e: Event) {
		e.preventDefault();
		window.open(source.origin);
	}

	return (
		<a class="article__info" href={article.url} target="_blank">
			<span class="article__info__title">{article.title}</span>

			<div>
				<b class="article__info__source" onClick={openOrigin}>
					[{source.hostname}]
				</b>
				<span>{date}</span>
			</div>
		</a>
	);
};

type ArticlesProps = {
	list: Snapshot<Article[]>;
};

const Articles = ({ list }: ArticlesProps) => {
	const isEmpty = !list.data || list.data.length === 0;

	return (
		<div class="articles">
			{list.error && <Log error>{list.error.message}</Log>}
			{list.loading && <Log>Loading...</Log>}

			{!list.loading && !list.error && isEmpty && <Log>No article found.</Log>}

			{!isEmpty && (
				<List list={(list.data ?? []).sort(dateDesc)}>
					{(article) => <ArticleInfo article={article} />}
				</List>
			)}
		</div>
	);
};

export default Articles;
