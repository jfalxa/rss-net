import { Fragment, h } from "preact";
import { useEffect, useState } from "preact/hooks";

import "./search.css";

import { Feed } from "../models";
import * as api from "../services/api";
import usePromise, { Snapshot } from "../hooks/promise";
import useLocation from "wouter-preact/use-location";
import { List, Log } from "./system";
import { FeedInfo } from "./feeds";

interface SearchHook {
	query: string;
	results: Snapshot<Feed[]>;
	onChange: (q: string) => void;
	onSearch: (q: string) => void;
	searchFeeds: (q: string) => Promise<Feed[] | void>;
}

export function useSearch(): SearchHook {
	const [, setLocation] = useLocation();

	const [query, onChange] = useState("");
	const [results, searchFeeds] = usePromise(api.searchFeeds);

	function onSearch() {
		if (query.length > 0) {
			searchFeeds(query);
			setLocation("/feeds/search");
		} else {
			setLocation("/feeds");
		}
	}

	return {
		query,
		results,
		onChange,
		onSearch,
		searchFeeds,
	};
}

type SearchFormProps = {
	query: string;
	onChange: (q: string) => void;
	onSearch: (q: string) => void;
};

export const SearchForm = ({ query, onChange, onSearch }: SearchFormProps) => {
	function onSubmit(e: Event) {
		e.preventDefault();
		onSearch(query);
	}

	return (
		<form class="search__form" onSubmit={onSubmit}>
			<input
				class="search__query"
				name="search-query"
				placeholder="Enter a feed URL or domain name..."
				value={query}
				onInput={(e: any) => onChange(e.target.value)}
			/>

			<button class="search__submit" type="submit" title="Submit search">
				[search]
			</button>
			<button
				class="search__clear"
				title="Clear search"
				onClick={() => onChange("")}
			>
				[clear]
			</button>
		</form>
	);
};

type SearchResultsProps = {
	query: string;
	results: Snapshot<Feed[]>;
	saving: Snapshot;
	onSave: (f: Feed) => void;
};

export const SearchResults = ({
	query,
	results,
	saving,
	onSave,
}: SearchResultsProps) => {
	const isEmpty = !results.data || results.data.length === 0;

	return (
		<div class="search-results">
			{results.loading && <Log>Loading...</Log>}
			{results.error && <Log error>{results.error.message}</Log>}
			{saving.error && <Log error>{saving.error.message}</Log>}

			{!results.loading && !results.error && isEmpty && (
				<Log>No feed found on this page</Log>
			)}

			{!isEmpty && (
				<List list={results.data ?? []}>
					{(feed) => (
						<Fragment>
							<FeedInfo feed={feed} />
							<button onClick={() => onSave(feed)}>[save]</button>
						</Fragment>
					)}
				</List>
			)}

			{!results.error && query.length > 0 && (
				<Log>
					<a
						class="search-results__external"
						href={`https://duckduckgo.com/?q=${query}+rss`}
						target="_blank"
					>
						{"=>"} Search related feeds on the web
					</a>
				</Log>
			)}
		</div>
	);
};
