import { h } from "preact";

import "./auth.css";

import { User } from "../models";
import * as api from "../services/api";
import usePromise, { Snapshot } from "../hooks/promise";
import { useEffect, useState } from "preact/hooks";
import { useLocation } from "wouter-preact";
import { Log } from "./system";

export interface AuthHook {
	step: number;
	user: Snapshot<User>;
	otping: Snapshot<any>;
	tokening: Snapshot<any>;
	canceling: Snapshot<any>;
	userInfo: () => Promise<any>;
	requestOTP: (e: string) => Promise<any>;
	requestToken: (e: string, c: string) => Promise<any>;
	cancelToken: () => Promise<any>;
}

export function useAuth(): AuthHook {
	const [step, setStep] = useState(0);
	const [, setLocation] = useLocation();

	const [user, userInfo] = usePromise(api.userInfo);

	const [otping, requestOTP] = usePromise(async (e: string) => {
		await api.requestOTP(e);
		setStep(1);
	});

	const [tokening, requestToken] = usePromise(async (e: string, c: string) => {
		await api.requestToken(e, c);
		setStep(2);
		userInfo();
		setLocation("/");
	});

	const [canceling, cancelToken] = usePromise(async () => {
		await api.cancelToken();
		setStep(0);
		userInfo();
		setLocation("/");
	});

	useEffect(() => {
		userInfo();
	}, []);

	return {
		step,
		user,
		otping,
		tokening,
		canceling,
		userInfo,
		requestOTP,
		requestToken,
		cancelToken,
	};
}

type AuthStep = {
	active: boolean;
	label: string;
	action: string;
	working: Snapshot;
	value: string;
	type?: string;
	hint: string;
	onChange: (v: string) => void;
};

const AuthStep = ({
	active,
	label,
	action,
	working,
	value,
	type,
	hint,
	onChange,
}: AuthStep) => (
	<div class="login__step">
		<label class="login__step__label" for={label.toLowerCase()}>
			{label}
		</label>

		<div>
			<input
				type={type}
				disabled={!active || working.loading}
				id={label.toLowerCase()}
				name={label.toLowerCase()}
				value={value}
				onChange={(e: any) => onChange(e.target.value)}
			/>

			<button type="submit" disabled={!active || working.loading}>
				[{action}]
			</button>

			<span class="login__step__hint">
				{"->"} {hint}
			</span>
		</div>
	</div>
);

type LoginProps = {
	step: number;
	otping: Snapshot;
	tokening: Snapshot;
	onRequestOTP: (e: string) => Promise<any>;
	onRequestToken: (e: string, c: string) => Promise<any>;
};

const Login = ({
	step,
	otping,
	tokening,
	onRequestOTP,
	onRequestToken,
}: LoginProps) => {
	const [email, setEmail] = useState("");
	const [code, setCode] = useState("");

	async function onSubmit(e: Event) {
		e.preventDefault();

		if (step === 0 && email.length > 0) {
			await onRequestOTP(email);
		}

		if (step === 1 && email.length > 0 && code.length > 0) {
			await onRequestToken(email, code);
		}
	}

	return (
		<div class="login">
			<form onSubmit={onSubmit} class="login__form">
				<AuthStep
					active={step === 0}
					label="Email"
					action="request code"
					value={email}
					onChange={setEmail}
					working={otping}
					hint="This will send a 6-digit code to your email address"
				/>

				<AuthStep
					active={step === 1}
					label="Code"
					action="login"
					value={code}
					onChange={setCode}
					working={tokening}
					hint="Enter the code you received to confirm your login"
				/>

				{otping.error && <Log error>{otping.error.message}</Log>}
				{tokening.error && <Log error>{tokening.error.message}</Log>}
			</form>
		</div>
	);
};

type LogoutProps = {
	canceling: Snapshot;
	onCancelToken: () => Promise<any>;
};

const Logout = ({ canceling, onCancelToken }: LogoutProps) => {
	const [, setLocation] = useLocation();

	useEffect(() => {
		if (confirm("Do you want to logout ?")) {
			onCancelToken().then(() => setLocation("/"));
		} else {
			setLocation("/");
		}
	}, []);

	return null;
};

export default {
	Login,
	Logout,
};
