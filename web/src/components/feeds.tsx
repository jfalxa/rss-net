import { Fragment, h } from "preact";
import { useEffect } from "preact/hooks";

import "./feeds.css";

import { Feed, SyncOp } from "../models";
import * as api from "../services/api";
import * as db from "../services/db";
import usePromise, { Snapshot } from "../hooks/promise";
import { AuthHook } from "./auth";
import { List } from "./system";

function toSyncOps(feeds: Feed[]): SyncOp[] {
	const ops = feeds.map<SyncOp | void>((f) => ({
		type: f.is_deleted ? "remove" : "add",
		data: f.feed_url,
	}));

	return ops.filter(Boolean) as SyncOp[];
}

export interface FeedsHook {
	list: Snapshot<Feed[]>;
	saving: Snapshot;
	forgetting: Snapshot;
	syncing: Snapshot;
	getAllFeeds: () => Promise<Feed[] | void>;
	saveFeed: (f: Feed) => Promise<any>;
	forgetFeed: (f: Feed) => Promise<any>;
	syncFeeds: () => Promise<any>;
}

export function useFeeds(auth: AuthHook): FeedsHook {
	const userID = auth.user.data?.user_id;

	const [list, getAllFeeds] = usePromise(async function ALLFEEDS() {
		const feeds = await db.getAllFeeds();
		return feeds.filter((f) => !f.is_deleted);
	});

	const [syncing, syncFeeds] = usePromise(async () => {
		if (!userID) {
			getAllFeeds();
			return;
		}

		const dbFeeds = await db.getAllFeeds();
		const ops = toSyncOps(dbFeeds);

		// send the list of local updates to the API
		// and reset the local database with latest api data
		const feeds = await api.syncUserFeed(userID, ops);
		await db.syncFeeds(feeds);

		getAllFeeds();
	}, [userID, getAllFeeds]);

	const [saving, saveFeed] = usePromise(
		async (source: Feed) => {
			const feed = await api.readFeed(source.feed_url);

			await db.addFeed(feed);
			syncFeeds();
		},
		[syncFeeds]
	);

	const [forgetting, forgetFeed] = usePromise(
		async (feed: Feed) => {
			if (confirm(`Remove "${feed.title}" ?`)) {
				await db.removeFeed(feed);
				syncFeeds();
			}
		},
		[syncFeeds]
	);

	useEffect(() => {
		syncFeeds();
	}, [syncFeeds]);

	return {
		list,
		saving,
		forgetting,
		syncing,
		getAllFeeds,
		saveFeed,
		forgetFeed,
		syncFeeds,
	};
}

type FeedInfoProps = {
	feed: Feed;
};

export const FeedInfo = ({ feed }: FeedInfoProps) => {
	const url = new URL(feed.feed_url);
	const title = feed.title.trim().length > 0 ? feed.title : url.pathname;

	return (
		<div class="feed-info">
			<span class="feed-info__title">{title}</span>

			<a class="feed-info__link" href={url.href} target="_blank">
				[{url.href}]
			</a>
		</div>
	);
};

type FeedsProps = {
	list: Snapshot<Feed[]>;
	onForget: (f: Feed) => void;
};

const Feeds = ({ list, onForget }: FeedsProps) => (
	<List list={list.data ?? []}>
		{(feed) => (
			<Fragment>
				<FeedInfo feed={feed} />
				<button onClick={() => onForget(feed)}>[remove]</button>
			</Fragment>
		)}
	</List>
);

export default Feeds;
