import { h } from "preact";
import cl from "clsx";

import "./system.css";

type ListProps<T> = Omit<h.JSX.HTMLAttributes<HTMLUListElement>, "list"> & {
	list: T[];
	children: (v: T, i: number) => h.JSX.Element;
};

export function List<T>({ list, children, ...props }: ListProps<T>) {
	return (
		<ul {...props} class={cl("list", props.class)}>
			{list.map((item, i) => (
				<li key={i} class="list__item">
					{children(item, i)}
				</li>
			))}
		</ul>
	);
}

type LogProps = h.JSX.HTMLAttributes<HTMLDivElement> & {
	error?: boolean;
};

export const Log = ({ error = false, children }: LogProps) => (
	<div class={cl("log", error && "log--error")}>{children}</div>
);

type HeaderProps = h.JSX.HTMLAttributes<HTMLElement> & {
	title: string;
};

export const Header = ({ title, children, ...props }: HeaderProps) => (
	<header {...props} class={cl("header", props.class)}>
		<h1 class="header__title">{title}</h1>
		{children}
	</header>
);

export const Top = () => (
	<button class="top" onClick={() => window.scrollTo({ top: 0 })}>
		[top]
	</button>
);
