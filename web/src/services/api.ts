import Http from "./http";
import { User, Feed, JSONFeed, SyncOp } from "../models";

// @ts-ignore
const HOST = process.env.HOST;

const AUTH = `${window.location.protocol}//auth.${HOST}`;
const RESOURCE = `${window.location.protocol}//api.${HOST}`;

function shouldRefreshToken(res: Response) {
	return res.status === 401;
}

export function refreshToken() {
	return window.fetch(AUTH + "/refresh-token", {
		method: "POST",
		credentials: "include",
	});
}

export const auth = new Http({
	host: AUTH,
	credentials: "include",
	refreshToken,
	shouldRefreshToken,
});

export const resource = new Http({
	host: RESOURCE,
	credentials: "include",
	refreshToken,
	shouldRefreshToken,
});

export function requestOTP(email: string) {
	return auth.post("/request-otp", { email });
}

export function requestToken(email: string, code: string) {
	return auth.post("/request-token", { email, code });
}

export function cancelToken() {
	return auth.post("/cancel-token");
}

export function userInfo() {
	return auth.get<User>("/user-info");
}

export function searchFeeds(query: string) {
	return resource.get<Feed[]>("/feeds/search", { query });
}

export function readFeed(url: string) {
	return resource.get<JSONFeed>("/feeds/read", { url });
}

export function getUserFeeds(userID: number) {
	return resource.get(`/users/${userID}/feeds`);
}

export function addUserFeed(userID: number, feed_url: string) {
	return resource.post(`/users/${userID}/feeds`, { feed_url });
}

export function removeUserFeed(userID: number, feed_url: string) {
	return resource.delete(`/users/${userID}/feeds`, { feed_url });
}

export function syncUserFeed(userID: number, operations: SyncOp[]) {
	return resource.patch<Feed[]>(`/users/${userID}/feeds`, operations);
}
