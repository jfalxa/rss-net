export type Envelope<T> = {
	data?: T;
	error?: number;
	message?: string;
};

export type HttpResponse<T> = Promise<Envelope<T>>;

export class HttpError extends Error {
	code: number;
	name: string = "HttpError";

	constructor(code: number, message: string) {
		super(`(E${code}) ${message}`);
		this.code = code;
	}
}

function searchParams<T>(search: T): string {
	// return a flat list of key/values fro the given object
	const entries = Object.entries(search).flatMap(([key, val]) => {
		if (Array.isArray(val)) {
			// allows for params in the form a=1&a=2&a=3&...
			return val.map((v) => [key, v]);
		} else {
			return [[key, val]];
		}
	});

	// and send them as search params
	return new URLSearchParams(entries).toString();
}

type Args = [
	string, // method
	string, // endpoint
	{ [key: string]: any }, // params
	RequestInit // options
];

type HttpOptions = RequestInit & {
	host: string;
	refreshToken?: () => Promise<Response>;
	shouldRefreshToken?: (r: Response) => boolean;
};

class Http {
	host: string;
	options: RequestInit;
	refreshToken?: () => Promise<Response>;
	shouldRefreshToken?: (r: Response) => boolean;

	constructor({
		host,
		refreshToken,
		shouldRefreshToken,
		...options
	}: HttpOptions) {
		this.host = host;
		this.options = options;
		this.refreshToken = refreshToken;
		this.shouldRefreshToken = shouldRefreshToken;
	}

	private async fetch<T>(input: string, init: RequestInit): Promise<T> {
		// start by actually trying to fetch the resource
		let res = await window.fetch(input, init);

		// if there's a unknwon server error, return it
		if (res.status === 500) {
			throw new HttpError(500, res.statusText);
		}

		// if there's an unauthorized response, try to refresh the token
		if (
			this.refreshToken &&
			this.shouldRefreshToken &&
			this.shouldRefreshToken(res)
		) {
			const refresh = await this.refreshToken();

			// repeat the request if refreshing was successful
			if (refresh.ok) {
				res = await window.fetch(input, init);
			}
		}

		// parse the result as JSON
		const json: Envelope<T> = await res.json();

		// if the request returned an error, throw it
		if (!res.ok || json.error) {
			throw new HttpError(
				json.error ?? res.status,
				json.message ?? res.statusText
			);
		}

		return json.data!;
	}

	private async method<T>(
		method: Args[0],
		pathname: Args[1],
		params: Args[2] = {},
		options: Args[3] = {}
	): Promise<T> {
		// prepare the target url
		const url = new URL(this.host + pathname);

		// setup search params for get requests
		if (method === "GET") {
			url.search = searchParams(params);
		}

		const input = url.toString();

		// prepare the request headers
		const headers = {
			Accept: "application/json",
			"Content-Type": "application/json",
			...this.options.headers,
			...options.headers,
		};

		// prepare the options for fetch
		const init: RequestInit = { ...this.options, ...options, method, headers };

		if (method !== "GET") {
			init.body = JSON.stringify(params);
		}

		// send the request
		return this.fetch(input, init);
	}

	get = <T>(pathname: Args[1], params?: Args[2], options?: Args[3]) =>
		this.method<T>("GET", pathname, params, options);

	post = <T>(pathname: Args[1], params?: Args[2], options?: Args[3]) =>
		this.method<T>("POST", pathname, params, options);

	put = <T>(pathname: Args[1], params?: Args[2], options?: Args[3]) =>
		this.method<T>("PUT", pathname, params, options);

	patch = <T>(pathname: Args[1], params?: Args[2], options?: Args[3]) =>
		this.method<T>("PATCH", pathname, params, options);

	delete = <T>(pathname: Args[1], params?: Args[2], options?: Args[3]) =>
		this.method<T>("DELETE", pathname, params, options);
}

export default Http;
