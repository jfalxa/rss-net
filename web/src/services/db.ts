import { DBSchema, openDB } from "idb";
import { Feed, Article, FeedArticle } from "../models";

interface RssSchema extends DBSchema {
	feed: {
		key: string;
		value: Feed;
	};

	article: {
		key: string;
		value: Article;
	};

	feed_article: {
		key: number;
		value: FeedArticle;
	};
}

function createDB() {
	return openDB<RssSchema>("rss-net", 1, {
		upgrade(db, oldVersion) {
			switch (oldVersion) {
				case 0:
					db.createObjectStore("feed", { keyPath: 'feed_url' });
					db.createObjectStore("article", { keyPath: 'url'});
					db.createObjectStore("feed_article", { keyPath: "id", autoIncrement: true }); // prettier-ignore
			}
		},
	});
}

export const db = createDB();

export async function getAllFeeds(): Promise<Feed[]> {
	return (await db).getAll("feed");
}

export async function getAllArticles(): Promise<Article[]> {
	return (await db).getAll("article");
}

export async function addFeed(data: Feed) {
	const feed: Feed = {
		title: data.title,
		description: data.description,
		feed_url: data.feed_url,
		icon: data.icon,
		is_synced: false,
		is_deleted: false,
	};

	return (await db).put("feed", feed);
}

export async function syncFeeds(feeds: Feed[]) {
	const tx = (await db).transaction(["feed"], "readwrite");
	const Feed = tx.objectStore("feed");

	await Feed.clear();

	for (const feed of feeds) {
		Feed.add({
			...feed,
			is_synced: true,
			is_deleted: false,
		});
	}

	await tx.done;
}

export async function addArticles(feed: Feed, articles: Article[]) {
	const tx = (await db).transaction(["article", "feed_article"], "readwrite");

	const Article = tx.objectStore("article");
	const FeedArticle = tx.objectStore("feed_article");

	for (const article of articles) {
		Article.put(article);
		FeedArticle.put({ feed: feed.feed_url, article: article.url }); // prettier-ignore
	}

	await tx.done;
}

export async function removeFeed(feed: Feed) {
	const tx = (await db).transaction(
		["feed", "article", "feed_article"],
		"readwrite"
	);

	const Feed = tx.objectStore("feed");
	const Article = tx.objectStore("article");
	const FeedArticle = tx.objectStore("feed_article");

	const allArticles = await FeedArticle.getAll();
	const feedArticles = allArticles.filter((fa) => fa.feed === feed.feed_url);

	feedArticles.forEach((a) => {
		Article.delete(a.article);
		FeedArticle.delete(a.id!);
	});

	if (!feed.is_synced) {
		Feed.delete(feed.feed_url);
	} else {
		Feed.put({
			...feed,
			is_deleted: true,
		});
	}

	await tx.done;
}
