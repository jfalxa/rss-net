package render

import (
	"encoding/json"
	"net/http"
)

// Envelope is the standard format for api responses
type Envelope struct {
	Data    interface{} `json:"data,omitempty"`
	Error   string      `json:"error,omitempty"`
	Message string      `json:"message,omitempty"`
}

// Response is a wrapper to help render responses
type Response struct {
	res http.ResponseWriter
}

// Render creates a new response helper
func Render(res http.ResponseWriter) Response {
	return Response{res: res}
}

// Data renders a JSON response
func (r Response) Data(data interface{}, params ...interface{}) {
	r.res.Header().Set("Content-Type", "application/json")

	if len(params) == 0 {
		r.res.WriteHeader(http.StatusOK)
	} else if status, ok := params[0].(int); ok {
		r.res.WriteHeader(status)
	}

	json.NewEncoder(r.res).Encode(Envelope{Data: data})
}

// Message renders a json with a message inside
func (r Response) Message(message string, params ...interface{}) {
	r.res.Header().Set("Content-Type", "application/json")

	if len(params) == 0 {
		r.res.WriteHeader(http.StatusOK)
	} else if status, ok := params[0].(int); ok {
		r.res.WriteHeader(status)
	}

	json.NewEncoder(r.res).Encode(Envelope{Message: message})
}

// Error renders an error in json format
func (r Response) Error(err error, params ...interface{}) {
	r.res.Header().Set("Content-Type", "application/json")

	if len(params) == 0 {
		r.res.WriteHeader(http.StatusBadRequest)
	} else if status, ok := params[0].(int); ok {
		r.res.WriteHeader(status)
	}

	json.NewEncoder(r.res).Encode(Envelope{Message: err.Error()})
}
