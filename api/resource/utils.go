package main

import (
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/mmcdole/gofeed"
)

func translate(data *gofeed.Feed, url string) Feed {
	feed := Feed{
		Title:       data.Title,
		Description: data.Description,
		FeedURL:     url,
		Items:       make([]Article, data.Len()),
	}

	if data.Image != nil {
		feed.Icon = data.Image.URL
	}

	for i, item := range data.Items {
		feed.Items[i] = Article{
			Title:         item.Title,
			Summary:       item.Description,
			URL:           item.Link,
			DatePublished: item.PublishedParsed.Format(time.RFC3339),
		}

		if item.Image != nil {
			feed.Items[i].Image = item.Image.URL
		}
	}

	return feed
}

func scrapHTML(response *http.Response, host string) ([]Feed, error) {
	document, err := goquery.NewDocumentFromReader(response.Body)

	if err != nil {
		return nil, err
	}

	links := document.Find("[href*=rss]")
	feeds := make([]Feed, links.Length())

	var linkErr error

	links.Each(func(i int, e *goquery.Selection) {
		link := e.AttrOr("href", "")

		if !strings.Contains(link, "http") {
			u, err := url.Parse(host)

			if err != nil {
				linkErr = err
			}

			u.Path = path.Join(u.Path, link)
			link = u.String()
		}

		feeds[i] = Feed{
			Title:   e.AttrOr("title", e.Text()),
			FeedURL: link,
		}
	})

	if linkErr != nil {
		return nil, linkErr
	}

	return feeds, nil
}
