package main

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/mmcdole/gofeed"
)

var schema = `
CREATE TABLE IF NOT EXISTS feed (
	feed_url TEXT PRIMARY KEY,
	title TEXT NOT NULL,
	description TEXT,
	icon TEXT
);

CREATE TABLE IF NOT EXISTS user_feed (
	user INTEGER,
	feed TEXT,
	PRIMARY KEY (user, feed),
	FOREIGN KEY (feed) REFERENCES feed(feed_url)
);`

// NewDB creates a new connection to the database
func NewDB() (*sqlx.DB, error) {
	db, err := sqlx.Connect("sqlite3", "db/rss-resource.db")

	if err != nil {
		return nil, err
	}

	_, err = db.Exec(schema)

	if err != nil {
		return nil, err
	}

	return db, nil
}

// FirstFeed finds the feed matching the given user id
func (c *Config) FirstFeed(feed *Feed, feedURL string) error {
	return c.DB.Get(feed, "SELECT * FROM feed WHERE feed_url=$1", feedURL)
}

// CreateFeed adds a new feed to the database
func (c *Config) CreateFeed(feed *Feed, value Feed) error {
	_, err := c.DB.NamedExec(`
		INSERT INTO feed (feed_url, title, description, icon) 
		VALUES (:feed_url, :title, :description, :icon)
	`, value)

	if err != nil {
		return err
	}

	feed.FeedURL = value.FeedURL
	feed.Title = value.Title
	feed.Description = value.Description
	feed.Icon = value.Icon

	return nil
}

// FirstOrCreateFeed tries to find the given feed or creates it if nothing was found
func (c *Config) FirstOrCreateFeed(feed *Feed, value Feed) error {
	err := c.FirstFeed(feed, value.FeedURL)

	if err != nil {
		if err = c.CreateFeed(feed, value); err != nil {
			return err
		}
	}

	return nil
}

// SelectUserFeeds gets the list of feeds of a user
func (c *Config) SelectUserFeeds(feeds *[]Feed, user uint64) error {
	err := c.DB.Select(feeds, `
		SELECT * 
		FROM feed 
		WHERE feed_url IN (
			SELECT feed 
			FROM user_feed
			WHERE user=$1
		)
	`, user)

	if err != nil {
		return err
	}

	return nil
}

// FirstOrCreateUserFeed adds a relation between a user and a feed if it does not already exist
func (c *Config) FirstOrCreateUserFeed(target *UserFeed, userFeed UserFeed) error {
	// var userFeed UserFeed
	err := c.DB.Get(target, "SELECT * FROM user_feed WHERE user=$1 AND feed=$2", userFeed.User, userFeed.Feed)

	if err == nil && target.User == userFeed.User && target.Feed == userFeed.Feed {
		return nil
	}

	parser := gofeed.NewParser()
	feed, err := parser.ParseURL(userFeed.Feed)

	if err != nil {
		return err
	}

	// create the feed if it does not exist
	if err := c.FirstOrCreateFeed(&Feed{}, translate(feed, userFeed.Feed)); err != nil {
		return err
	}

	_, err = c.DB.Exec(`
		INSERT INTO user_feed (user, feed) 
		VALUES ($1, $2)
	`, userFeed.User, userFeed.Feed)

	if err != nil {
		return err
	}

	return nil
}

// DeleteUserFeed adds a relation between a user and a feed
func (c *Config) DeleteUserFeed(userFeed UserFeed) error {
	_, err := c.DB.Exec(`
		DELETE FROM user_feed 
		WHERE user=$1 AND feed=$2
	`, userFeed.User, userFeed.Feed)

	if err != nil {
		return err
	}

	return nil
}
