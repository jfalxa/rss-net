package main

import (
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"

	"rss-api/jwt"
)

func main() {
	r := chi.NewRouter()
	c := NewConfig()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"http://" + os.Getenv("HOST"), "https://" + os.Getenv("HOST")},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Set-Cookie", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/feeds/read", ReadFeed)
	r.Get("/feeds/search", SearchFeed)

	r.With(jwt.TokenAuth).Get("/users/{userID}/feeds", c.GetUserFeeds)
	r.With(jwt.TokenAuth).Post("/users/{userID}/feeds", c.AddUserFeed)
	r.With(jwt.TokenAuth).Delete("/users/{userID}/feeds", c.RemoveUserFeed)
	r.With(jwt.TokenAuth).Patch("/users/{userID}/feeds", c.SyncUserFeeds)

	http.ListenAndServe(":8080", r)
}
