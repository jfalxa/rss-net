package main

// Feed !
type Feed struct {
	Title       string    `json:"title" db:"title"`
	Description string    `json:"description" db:"description"`
	FeedURL     string    `json:"feed_url" db:"feed_url"`
	Icon        string    `json:"icon" db:"icon"`
	Items       []Article `json:"items,omitempty"`
}

// Article !
type Article struct {
	Title         string `json:"title"`
	Summary       string `json:"summary"`
	URL           string `json:"url"`
	DatePublished string `json:"date_published"`
	Image         string `json:"image"`
}

// UserFeed !
type UserFeed struct {
	User uint64 `db:"user"`
	Feed string `db:"feed"`
}
