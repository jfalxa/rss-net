package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"rss-api/jwt"
	"rss-api/render"
)

func userID(req *http.Request) (uint64, error) {
	return strconv.ParseUint(chi.URLParam(req, "userID"), 10, 64)
}

// GetUserFeeds returns the list of feeds from the given user
func (c *Config) GetUserFeeds(w http.ResponseWriter, r *http.Request) {
	userID, err := userID(r)
	res := render.Render(w)

	if err != nil {
		res.Error(err)
		return
	}

	authUserID, err := jwt.GetUserID(r)

	if err != nil {
		res.Error(err)
		return
	}

	if authUserID != userID {
		res.Message("User access is forbidden", http.StatusForbidden)
		return
	}

	feeds := []Feed{}
	err = c.SelectUserFeeds(&feeds, userID)

	if err != nil {
		res.Error(err)
		return
	}

	res.Data(feeds)
}

// AddUserFeed adds a feed to the user's list
func (c *Config) AddUserFeed(w http.ResponseWriter, r *http.Request) {
	userID, err := userID(r)
	res := render.Render(w)

	if err != nil {
		res.Error(err)
		return
	}

	authUserID, err := jwt.GetUserID(r)

	if err != nil {
		res.Error(err)
		return
	}

	if authUserID != userID {
		res.Message("User modification is forbidden", http.StatusForbidden)
		return
	}

	var body Feed

	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		res.Error(err)
		return
	}

	if body.FeedURL == "" {
		res.Message("Missing feed url", http.StatusBadRequest)
		return
	}

	userFeed := UserFeed{User: userID, Feed: body.FeedURL}
	err = c.FirstOrCreateUserFeed(&UserFeed{}, userFeed)

	if err != nil {
		res.Error(err)
		return
	}

	res.Message("Feed added successfully")
}

// RemoveUserFeed adds a feed to the user's list
func (c *Config) RemoveUserFeed(w http.ResponseWriter, r *http.Request) {
	userID, err := userID(r)
	res := render.Render(w)

	if err != nil {
		res.Error(err)
		return
	}

	authUserID, err := jwt.GetUserID(r)

	if err != nil {
		res.Error(err)
		return
	}

	if authUserID != userID {
		res.Message("User modification is forbidden", http.StatusForbidden)
		return
	}

	var body Feed

	if err = json.NewDecoder(r.Body).Decode(&body); err != nil {
		res.Error(err)
		return
	}

	if body.FeedURL == "" {
		res.Message("Missing feed url", http.StatusBadRequest)
		return
	}

	userFeed := UserFeed{User: userID, Feed: body.FeedURL}
	err = c.DeleteUserFeed(userFeed)

	if err != nil {
		res.Error(err)
		return
	}

	res.Message("Feed removed successfully")
}

// SyncOp holds info about an atomic change to the database
type SyncOp struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

// SyncUserFeeds gets a list of modifications, updates the database accordingly and returns the updated list
func (c *Config) SyncUserFeeds(w http.ResponseWriter, r *http.Request) {
	userID, err := userID(r)
	res := render.Render(w)

	if err != nil {
		res.Error(err)
		return
	}

	authUserID, err := jwt.GetUserID(r)

	if err != nil {
		res.Error(err)
		return
	}

	if authUserID != userID {
		res.Message("User modification is forbidden", http.StatusForbidden)
		return
	}

	body := []SyncOp{}

	if err = json.NewDecoder(r.Body).Decode(&body); err != nil {
		res.Error(err)
		return
	}

	for _, op := range body {
		userFeed := UserFeed{User: userID, Feed: op.Data}

		if op.Type == "add" {
			if err := c.FirstOrCreateUserFeed(&UserFeed{}, userFeed); err != nil {
				res.Error(err)
				return
			}
		}

		if op.Type == "remove" {
			if err := c.DeleteUserFeed(userFeed); err != nil {
				res.Error(err)
				return
			}
		}
	}

	feeds := []Feed{}

	if err := c.SelectUserFeeds(&feeds, userID); err != nil {
		res.Error(err)
		return
	}

	res.Data(feeds)
}
