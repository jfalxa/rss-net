package main

import (
	"net/http"
	"net/url"
	"rss-api/render"
	"strings"

	"github.com/mmcdole/gofeed"
)

// SearchFeed looks for feed urls at the given address or tries to check if the url is already pointing to a feed
func SearchFeed(w http.ResponseWriter, r *http.Request) {
	var feeds []Feed
	res := render.Render(w)

	query := r.URL.Query().Get("query")

	if !strings.Contains(query, "http") {
		query = "https://" + query
	}

	u, err := url.Parse(query)

	if err != nil {
		res.Error(err)
		return
	}

	feedURL := u.String()
	response, err := http.Get(feedURL)

	if err != nil {
		res.Error(err)
		return
	}

	defer response.Body.Close()

	contentType := response.Header.Get("Content-Type")

	if strings.Contains(contentType, "text/html") {
		feeds, err = scrapHTML(response, feedURL)

		if err != nil {
			res.Error(err)
			return
		}
	} else {
		parser := gofeed.NewParser()
		feed, err := parser.Parse(response.Body)

		if err != nil {
			res.Error(err)
			return
		}

		feed.Items = nil
		feeds = []Feed{translate(feed, feedURL)}
	}

	res.Data(feeds)
}

// ReadFeed parses the feed at the given url and returns it as JSON
func ReadFeed(w http.ResponseWriter, r *http.Request) {
	url := r.URL.Query().Get("url")
	res := render.Render(w)

	if url == "" {
		res.Message("Missing feed url", http.StatusBadRequest)
		return
	}

	parser := gofeed.NewParser()
	feed, err := parser.ParseURL(url)

	if err != nil {
		res.Error(err)
		return
	}

	res.Data(translate(feed, url))
}
