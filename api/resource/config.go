package main

import (
	"github.com/jmoiron/sqlx"
)

// Envelope is the standard format for api responses
type Envelope struct {
	Data    interface{} `json:"data,omitempty"`
	Error   string      `json:"error,omitempty"`
	Message string      `json:"message,omitempty"`
}

// Config holds data shared by the whole service
type Config struct {
	DB *sqlx.DB
}

// NewConfig inits the env variable with the right config
func NewConfig() *Config {
	db, err := NewDB()

	if err != nil {
		panic("failed to connect database")
	}

	return &Config{
		DB: db,
	}
}
