module rss-api

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.1.1
	github.com/jmoiron/sqlx v1.3.1
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/mmcdole/gofeed v1.1.0
)
