package jwt

import (
	"context"
	"errors"
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"

	"rss-api/render"
)

type key string

var secret = []byte(os.Getenv("JWT_SECRET"))

var tokenDuration = 5 * time.Minute            // 5 minutes
var refreshTokenDuration = 30 * 24 * time.Hour // 30 days

var tokenDurationSec = int(tokenDuration.Seconds())
var refreshTokenDurationSec = int(refreshTokenDuration.Seconds())

// UserID holds the name of the userID key that will be set in the context by the jwt middleware
const UserID key = "userID"

// Envelope holds the standard api response format
type Envelope struct {
	Data    interface{} `json:"data,omitempty"`
	Error   string      `json:"error,omitempty"`
	Message string      `json:"message,omitempty"`
}

// UserClaims holds the information that should be shared by a token
type UserClaims struct {
	UserID uint64 `json:"user_id"`
	jwt.StandardClaims
}

func keyFunc(token *jwt.Token) (interface{}, error) {
	return secret, nil
}

func newJWT(userID uint64, duration time.Duration) (string, error) {
	expirationTime := time.Now().Add(duration)

	jwtClaims := UserClaims{
		UserID: userID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims)
	return token.SignedString(secret)
}

// NewToken generates a JWT token with a short life
func NewToken(userID uint64) (string, error) {
	return newJWT(userID, tokenDuration)
}

// NewRefreshToken generates a JWT token with a longer life
func NewRefreshToken(userID uint64) (string, error) {
	return newJWT(userID, refreshTokenDuration)
}

// ParseToken validates and extracts data from a JWT token string
func ParseToken(signedToken string) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(signedToken, &UserClaims{}, keyFunc)
	return token, err
}

// SetTokens creates new tokens for the given userID and sets them as cookies
func SetTokens(res http.ResponseWriter, userID uint64) error {
	// create a new access token
	token, err := NewToken(userID)

	if err != nil {
		return err
	}

	// create a new refresh token
	refreshToken, err := NewRefreshToken(userID)

	if err != nil {
		return err
	}

	host := os.Getenv("HOST")

	tokenCookie := &http.Cookie{
		Name:     "token",
		Value:    token,
		Domain:   host,
		Path:     "/",
		MaxAge:   tokenDurationSec,
		HttpOnly: true,
		Secure:   false,
		SameSite: http.SameSiteStrictMode,
	}

	refreshTokenCookie := &http.Cookie{
		Name:     "refresh-token",
		Value:    refreshToken,
		Domain:   host,
		Path:     "/",
		MaxAge:   refreshTokenDurationSec,
		SameSite: http.SameSiteStrictMode,
		HttpOnly: true,
		Secure:   false,
	}

	http.SetCookie(res, tokenCookie)
	http.SetCookie(res, refreshTokenCookie)

	return nil
}

// UnsetTokens replaces existing token cookies by empty ones
func UnsetTokens(res http.ResponseWriter) error {
	host := os.Getenv("HOST")

	tokenCookie := &http.Cookie{
		Name:     "token",
		Value:    "",
		Domain:   host,
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
		Secure:   false,
		SameSite: http.SameSiteStrictMode,
	}

	refreshTokenCookie := &http.Cookie{
		Name:     "refresh-token",
		Value:    "",
		Domain:   host,
		Path:     "/",
		MaxAge:   -1,
		SameSite: http.SameSiteStrictMode,
		HttpOnly: true,
		Secure:   false,
	}

	http.SetCookie(res, tokenCookie)
	http.SetCookie(res, refreshTokenCookie)

	return nil
}

// GetUserID returns the userID currently stored in the context
func GetUserID(req *http.Request) (uint64, error) {
	ctx := req.Context()
	userID, ok := ctx.Value(UserID).(uint64)

	if !ok {
		return 0, errors.New("missing userID")
	}

	return userID, nil
}

// TokenAuth is a middleware that validates the tokens found in the request cookies
func TokenAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		reqToken, err := req.Cookie("token")
		r := render.Render(res)

		// check that the cookie exists
		if err != nil {
			r.Error(err, http.StatusUnauthorized)
			return
		}

		// parse the JWT token
		token, err := ParseToken(reqToken.Value)

		// check that the token is valid
		if err != nil {
			r.Error(err, http.StatusUnauthorized)
			return
		}

		if !token.Valid {
			r.Message("Token is invalid", http.StatusUnauthorized)
			return
		}

		claims, ok := token.Claims.(*UserClaims)

		// check that the token has the required information
		if !ok {
			r.Message("Token claims are invalid", http.StatusUnauthorized)
			return
		}

		// if everything's alright, save the user ID in the context for easy access
		ctx := context.WithValue(req.Context(), UserID, claims.UserID)

		// pass the request to the next middleware with the new context
		next.ServeHTTP(res, req.WithContext(ctx))
	})
}
