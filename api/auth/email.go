package main

import (
	"fmt"
	"net/smtp"
	"os"
)

// SendEmail sends an email with the given from, to and message
func SendEmail(from string, to []string, msg []byte) error {
	// grab smtp config from environment
	host := os.Getenv("SMTP_HOST")
	port := os.Getenv("SMTP_PORT")
	login := os.Getenv("SMTP_LOGIN")
	password := os.Getenv("SMTP_PASSWORD")

	// Set up authentication information.
	auth := smtp.PlainAuth("", login, password, host)

	// format smtp address
	smtpAddress := fmt.Sprintf("%s:%v", host, port)

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	return smtp.SendMail(smtpAddress, auth, from, to, msg)
}
