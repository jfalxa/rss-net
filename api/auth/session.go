package main

import (
	"encoding/json"
	"net/http"
	"os"

	"rss-api/jwt"
	"rss-api/render"
)

type requestCodeReq struct {
	Email string `json:"email"`
}

// RequestOTP sends an email with a code to allow the user to request a token later
func (c *Config) RequestOTP(w http.ResponseWriter, r *http.Request) {
	var body requestCodeReq
	res := render.Render(w)

	// parse json from request body
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		res.Error(err)
		return
	}

	if body.Email == "" {
		res.Message("Missing email", http.StatusBadRequest)
		return
	}

	code, err := GenerateOTP(6)

	if err != nil {
		res.Error(err)
		return
	}

	// From address
	from := os.Getenv("SMTP_FROM")

	// Array of recipients address
	to := []string{body.Email}

	// Create a message and convert it into bytes
	msg := []byte("To: " + body.Email + "\r\n" +
		"From: " + from + "\r\n" +
		"Subject: Your access code to feed.omuyo.com\r\n" +
		"\r\n" +
		"Please enter the following code on the authentication page: " + code + "\r\n")

	// Call the sendEmail function
	if err := SendEmail(from, to, msg); err != nil {
		res.Error(err)
		return
	}

	// save the generated code for this email inside the live memory
	c.Codes[body.Email] = code

	res.Message("Email sent successfully")
}

type requestTokenReq struct {
	Email string `json:"email"`
	Code  string `json:"code"`
}

// RequestToken uses a code previously sent by the API to generate a new token for the requesting user
func (c *Config) RequestToken(w http.ResponseWriter, r *http.Request) {
	var body requestTokenReq
	res := render.Render(w)

	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		res.Error(err)
		return
	}

	if body.Email == "" || body.Code == "" {
		res.Message("Missing parameters", http.StatusBadRequest)
		return
	}

	// check that the code is valid for the given email
	if body.Code != c.Codes[body.Email] {
		res.Message("Invalid access code", http.StatusBadRequest)
		return
	}

	// clear code after use
	delete(c.Codes, body.Email)

	// create the user in the database if it doesn't already exist
	var user User

	if err := c.FirstOrCreateUser(&user, body.Email); err != nil {
		res.Error(err)
		return
	}

	// generate new tokens for this user ID and set them as cookies
	if err := jwt.SetTokens(w, user.UserID); err != nil {
		res.Error(err)
		return
	}

	res.Message("User authorized")
}

// CancelToken deletes the token cookies from the server and client
func (c *Config) CancelToken(w http.ResponseWriter, r *http.Request) {
	res := render.Render(w)

	if err := jwt.UnsetTokens(w); err != nil {
		res.Error(err)
		return
	}

	res.Message("User logged out")
}

// RefreshToken uses a refresh token to generate a new access token for the requesting user
func (c *Config) RefreshToken(w http.ResponseWriter, r *http.Request) {
	reqRefreshToken, err := r.Cookie("refresh-token")
	res := render.Render(w)

	// parse refresh token request body
	if err != nil {
		res.Error(err)
		return
	}

	refreshToken, err := jwt.ParseToken(reqRefreshToken.Value)

	// check that the refresh token is valid
	if err != nil {
		res.Error(err)
		return
	}

	claims, ok := refreshToken.Claims.(*jwt.UserClaims)

	if !ok {
		res.Message("Invalid claims", http.StatusBadRequest)
	}

	// generate new tokens for this user ID and set them as cookies
	if err := jwt.SetTokens(w, claims.UserID); err != nil {
		res.Error(err)
		return
	}

	res.Message("Refreshed token")
}

// UserInfo sends the user information matching the current token passed as cookie
func (c *Config) UserInfo(w http.ResponseWriter, r *http.Request) {
	userID, err := jwt.GetUserID(r)
	res := render.Render(w)

	if err != nil {
		res.Error(err)
		return
	}

	// find the first user matching this user id
	var user User
	err = c.FirstUser(&user, userID)

	// fail the request if the user does not exist
	if err != nil {
		res.Error(err)
		return
	}

	// return the user matching the token
	res.Data(user)
}
