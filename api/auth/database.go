package main

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var schema = `
CREATE TABLE IF NOT EXISTS user (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
    email TEXT UNIQUE NOT NULL
)`

// User holds the information of an authorized user
type User struct {
	UserID uint64 `json:"user_id" db:"user_id"`
	Email  string `json:"email" db:"email"`
}

// NewDB creates a new connection to the database
func NewDB() (*sqlx.DB, error) {
	db, err := sqlx.Connect("sqlite3", "db/rss-auth.db")

	if err != nil {
		return nil, err
	}

	if _, err := db.Exec(schema); err != nil {
		return nil, err
	}

	return db, nil
}

// FirstUser finds the user matching the given user id
func (c *Config) FirstUser(user *User, userID uint64) error {
	return c.DB.Get(user, "SELECT * FROM user WHERE user_id=$1", userID)
}

// CreateUser adds a new user to the database
func (c *Config) CreateUser(user *User, email string) error {
	result, err := c.DB.Exec("INSERT INTO user (email) VALUES ($1)", email)

	if err != nil {
		return err
	}

	userID, err := result.LastInsertId()

	if err != nil {
		return err
	}

	user.UserID = uint64(userID)
	user.Email = email

	return nil
}

// FirstOrCreateUser tries to find the given user or creates it if nothing was found
func (c *Config) FirstOrCreateUser(user *User, email string) error {
	err := c.DB.Get(user, "SELECT * FROM user WHERE email=$1", email)

	if err != nil {
		if err := c.CreateUser(user, email); err != nil {
			return err
		}
	}

	return nil
}
