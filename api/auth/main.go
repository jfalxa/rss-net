package main

import (
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"

	"rss-api/jwt"
)

func main() {
	r := chi.NewRouter()
	c := NewConfig()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"http://" + os.Getenv("HOST"), "https://" + os.Getenv("HOST")},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Set-Cookie", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Post("/request-otp", c.RequestOTP)
	r.Post("/request-token", c.RequestToken)
	r.Post("/cancel-token", c.CancelToken)
	r.Post("/refresh-token", c.RefreshToken)
	r.With(jwt.TokenAuth).Get("/user-info", c.UserInfo)

	http.ListenAndServe(":8080", r)
}
