package main

import "crypto/rand"

const otpChars = "0123456789"

// GenerateOTP creates a random one time password of the given length
func GenerateOTP(length int) (string, error) {
	buffer := make([]byte, length)
	_, err := rand.Read(buffer)

	if err != nil {
		return "", err
	}

	max := len(otpChars)

	for i := 0; i < length; i++ {
		buffer[i] = otpChars[int(buffer[i])%max]
	}

	return string(buffer), nil
}
