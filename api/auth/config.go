package main

import (
	"github.com/jmoiron/sqlx"
)

// Config holds data shared by the whole service
type Config struct {
	DB    *sqlx.DB
	Codes map[string]string
}

// NewConfig inits the env variable with the right config
func NewConfig() *Config {
	db, err := NewDB()

	if err != nil {
		panic("failed to connect database")
	}

	return &Config{
		DB:    db,
		Codes: make(map[string]string),
	}
}
